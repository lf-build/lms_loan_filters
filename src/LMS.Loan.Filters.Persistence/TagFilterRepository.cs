﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using LMS.Loan.Filters.Abstractions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Filters.Persistence
{
    public class TagFilterRepository : MongoRepository<IFilterViewTags, FilterViewTags>, ITagFilterRepository
    {
        static TagFilterRepository()
        {
            BsonClassMap.RegisterClassMap<FilterViewTags>(map =>
            {
                map.AutoMap();
                var type = typeof(FilterViewTags);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public TagFilterRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "loan-filters-tags")
        {
            CreateIndexIfNotExists("entityId", Builders<IFilterViewTags>.IndexKeys.Ascending(i => i.LoanNumber));

        }
        public Task<IEnumerable<IFilterViewTags>> GetByTags(IEnumerable<string> tags)
        {
            return Task.FromResult<IEnumerable<IFilterViewTags>>
            (
                OrderIt(from db in Query.ToList()
                        from dbTags in db.Tags
                        where (from tag in tags select tag).Contains(dbTags.TagName)
                        select db).Distinct()
            );
        }

        public Task<IEnumerable<IFilterViewTags>> GetByNotInTags(IEnumerable<string> tags)
        {
            var filters = new List<FilterDefinition<IFilterViewTags>>();
            filters.Add(Builders<IFilterViewTags>.Filter.Eq("TenantId", TenantService.Current.Id));
            filters.Add(Builders<IFilterViewTags>.Filter.Nin("Tags.TagName", tags));

            var finalFilter = Builders<IFilterViewTags>.Filter.And(filters);
            return Task.FromResult<IEnumerable<IFilterViewTags>>
            (
                Collection.Find(finalFilter).ToEnumerable<IFilterViewTags>()
            );
        }

        public Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag)
        {
            return Task.FromResult<IEnumerable<IFilterViewTags>>
            (
                OrderIt(from db in Query.ToList()
                        from dbTags in db.Tags
                        where (dbTags.TagName == tag)
                        select db).Distinct()
            );
        }
        public Task<int> GetCountByTag(IEnumerable<string> tags)
        {
            return Task.FromResult<int>
            (
                OrderIt(from db in Query.ToList()
                        from dbTags in db.Tags
                        where (from tag in tags select tag).Contains(dbTags.TagName)
                        select db).Distinct().Count()
            );
        }
        public IFilterViewTags GetTagsForApplication(string loanNumber)
        {
            return Query.SingleOrDefault(x => x.LoanNumber == loanNumber);
        }
        public void AddTag(string loanNumber, string tag)
        {
            var tenantId = TenantService.Current.Id;
            var tagRecord = Query.FirstOrDefault(x => x.LoanNumber == loanNumber && x.TenantId == tenantId);
            var tags = tagRecord?.Tags.Select(x => x.TagName).ToList();
            bool addTag = true;
            if (tags != null)
            {
                if (tags.Contains(tag))
                {
                    addTag = false;
                }
            }

            if (addTag == true)
            {
                Collection.UpdateOne(s =>
                        s.LoanNumber == loanNumber &&
                        s.TenantId == tenantId,
                        new UpdateDefinitionBuilder<IFilterViewTags>()
                            .AddToSet<TagInfo>(a => a.Tags, new TagInfo() { TagName = tag, TaggedOn = new TimeBucket(DateTimeOffset.Now) })
                    , new UpdateOptions() { IsUpsert = true });
            }
        }
        public void RemoveTag(string loanNumber, string tag)
        {
            var tenantId = TenantService.Current.Id;

            var tagRecord = Query.FirstOrDefault(x => x.LoanNumber == loanNumber && x.TenantId == tenantId);
            var tagInfo = tagRecord?.Tags?.FirstOrDefault(x => x.TagName == tag);

            if (tagInfo != null)
            {
                Collection.UpdateOne(s =>
                        s.LoanNumber == loanNumber &&
                        s.TenantId == tenantId,
                        new UpdateDefinitionBuilder<IFilterViewTags>()
                        .Pull<TagInfo>(a => a.Tags, tagInfo)
                    , new UpdateOptions() { IsUpsert = true });
            }
        }
        public void ClearAllTags(string loanNumber)
        {
            var tenantId = TenantService.Current.Id;
            var tagRecord = Query.FirstOrDefault(x => x.LoanNumber == loanNumber && x.TenantId == tenantId);
            var tags = tagRecord.Tags;

            if (tagRecord != null)
            {
                Collection.UpdateOne(s =>
                        s.LoanNumber == loanNumber &&
                        s.TenantId == tenantId,
                        new UpdateDefinitionBuilder<IFilterViewTags>()
                        .PullAll<TagInfo>(a => a.Tags, tags)
                    , new UpdateOptions() { IsUpsert = true });
            }
        }
        private IQueryable<IFilterViewTags> OrderIt(IEnumerable<IFilterViewTags> filterTagsView)
        {
            var tenantId = TenantService.Current.Id;
            return filterTagsView.TakeWhile(x => x.TenantId.ToLower() == tenantId.ToLower()).OrderBy(x => x.LoanNumber).AsQueryable();
        }

    }
}