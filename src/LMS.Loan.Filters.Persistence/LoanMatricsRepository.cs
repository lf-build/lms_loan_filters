﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Encryption;
using LMS.Loan.Filters.Abstractions;
using LMS.LoanAccounting;
using LendFoundry.StatusManagement;

namespace LMS.Loan.Filters.Persistence
{
    public class LoanMetricsRepository : MongoRepository<ILoanMetrics, LoanMetrics>, ILoanMetricsRepository
    {
        static LoanMetricsRepository()
        {
            BsonClassMap.RegisterClassMap<LoanMetrics>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanMetrics);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<MetricsDetail>(map =>
            {
                map.AutoMap();
                var type = typeof(MetricsDetail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public LoanMetricsRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "loan-metrics")
        {
        }
        public async Task AddOrUpdate(ILoanMetrics metrics)
        {
            if (metrics == null)
                throw new ArgumentNullException(nameof(metrics));

            Expression<Func<ILoanMetrics, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.LoanNumber == metrics.LoanNumber;

            var existingView = Query
                .Where(v => v.LoanNumber == metrics.LoanNumber && v.TenantId == TenantService.Current.Id)
                .FirstOrDefault();

            metrics.TenantId = TenantService.Current.Id;
            if (existingView != null && !string.IsNullOrEmpty(existingView.Id))
                metrics.Id = existingView.Id;
            else
                metrics.Id = ObjectId.GenerateNewId().ToString();

            await Collection.FindOneAndReplaceAsync(query, metrics, new FindOneAndReplaceOptions<ILoanMetrics, LoanMetrics> { IsUpsert = true });
        }

        public async Task<ILoanMetrics> GetMetrics(string LoanNumber)
        {
            return await Task.Run(() => Query.FirstOrDefaultAsync(x => x.LoanNumber == LoanNumber));
        }

        public async Task<List<ILoanMetrics>> GetAllMetrics()
        {
            return await Query.ToListAsync();
        }


    }
}