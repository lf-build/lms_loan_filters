﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Encryption;
using LMS.Loan.Filters.Abstractions;
using LMS.LoanAccounting;
using LendFoundry.StatusManagement;

namespace LMS.Loan.Filters.Persistence
{
  public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
  {
    static FilterViewRepository()
    {
      BsonClassMap.RegisterClassMap<FilterView>(map =>
      {
        map.AutoMap();
        var type = typeof(FilterView);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(true);
      });

      BsonClassMap.RegisterClassMap<SubStatus>(map =>
      {
        map.AutoMap();
        var type = typeof(SubStatus);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(true);
      });
      BsonClassMap.RegisterClassMap<PBOC>(map =>
      {
        map.AutoMap();
        var type = typeof(PBOC);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

      BsonClassMap.RegisterClassMap<PBOT>(map =>
      {
        map.AutoMap();
        var type = typeof(PBOT);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });
      BsonClassMap.RegisterClassMap<ExcessMoney>(map =>
      {
        map.AutoMap();
        var type = typeof(ExcessMoney);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

      BsonClassMap.RegisterClassMap<PaymentInfo>(map =>
      {
        map.AutoMap();
        var type = typeof(PaymentInfo);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

      BsonClassMap.RegisterClassMap<ChargeOff>(map =>
      {
        map.AutoMap();
        var type = typeof(ChargeOff);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

      BsonClassMap.RegisterClassMap<Due>(map =>
      {
        map.AutoMap();
        var type = typeof(Due);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

      BsonClassMap.RegisterClassMap<PayOff>(map =>
      {
        map.AutoMap();
        var type = typeof(PayOff);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

      BsonClassMap.RegisterClassMap<SellOff>(map =>
      {
        map.AutoMap();
        var type = typeof(SellOff);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

      BsonClassMap.RegisterClassMap<PaymentDetails>(map =>
      {
        map.AutoMap();
        var type = typeof(PaymentDetails);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

      BsonClassMap.RegisterClassMap<TimeBucket>(map =>
      {
        map.AutoMap();
        map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
        var type = typeof(TimeBucket);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });
      BsonClassMap.RegisterClassMap<PriorInterest>(map =>
      {
        map.AutoMap();
        var type = typeof(PriorInterest);
        map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
        map.SetIsRootClass(false);
      });

    }

    public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
        : base(tenantService, configuration, "loan-filters")
    {
    }

    public void AddOrUpdate(IFilterView view)
    {
      if (view == null)
        throw new ArgumentNullException(nameof(view));

      Expression<Func<IFilterView, bool>> query = l =>
          l.TenantId == TenantService.Current.Id &&
          l.LoanNumber == view.LoanNumber;

      var existingView = Query
          .Where(v => v.LoanNumber == view.LoanNumber && v.TenantId == TenantService.Current.Id)
          .FirstOrDefault();

      view.TenantId = TenantService.Current.Id;
      if (existingView != null && !string.IsNullOrEmpty(existingView.Id))
        view.Id = existingView.Id;
      else
        view.Id = ObjectId.GenerateNewId().ToString();

      Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
    }

    public IEnumerable<IFilterView> GetAll()
    {
      return OrderIt(Query);
    }

    public async Task<IFilterView> GetFilterView(string loanNumber)
    {
      return await Task.Run(() => Query.Where(filterView => filterView.TenantId == TenantService.Current.Id && filterView.LoanNumber == loanNumber).FirstOrDefault());
    }

    private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
    {
      return filterView.OrderBy(x => x.StatusCode)
                       .ThenBy(x => x.LoanOnboardedDate).AsQueryable();
    }

    public Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
    {
      return Task.FromResult<IEnumerable<IFilterView>>
      (
          Query.Where(x => statuses.Contains(x.StatusCode))
      );
    }

    public Task<IEnumerable<IFilterView>> GetLoansByProductId(string productid)
    {
      return Task.FromResult<IEnumerable<IFilterView>>
     (
         Query.Where(x => x.LoanProductId == productid)
     );
      // return await Query.Where(x => x.LoanProductId == productid).ToListAsync();
    }

    public async Task<List<IFilterView>> GetLoansByLoanNumbers(List<string> loanNumbers)
    {
      return await Query.Where(x => loanNumbers.Contains(x.LoanNumber)).ToListAsync();
    }

    public async Task<List<IFilterView>> GetCollectionData(LoanCollectionRequest loanCollectionRequest)
    {

      var data = await Query.Where(l => l.CollectionDetails.Any(c =>
                                       c.DateOfTransition.Time >= loanCollectionRequest.FromDate.Date
                                       &&
                                       c.DateOfTransition.Time <= loanCollectionRequest.ToDate.Date)).ToListAsync();
      return data;
    }

      public async Task<List<IFilterView>> GetFundingData(LoanCollectionRequest loanCollectionRequest)
    {

      var data = await Query.Where(l => l.FundedDate.Time >= loanCollectionRequest.FromDate.Date
                                       &&
                                       l.FundedDate.Time <= loanCollectionRequest.ToDate.Date).ToListAsync();
      return data;
    }
    
    public async Task<List<IFilterView>> GetByBorrowerId(string borrowerId)
    {
      return await Query.Where(loanInformation => loanInformation.BorrowerId == borrowerId).ToListAsync();
    }

    public async Task<IEnumerable<IFilterView>> GetLoansByOnBoardedByDate(int year, int month, int day)
    {
      var date = new DateTime(year,month,day).ToString("yyyy-MM-dd") + "-d";
      var data = await Query.Where(l => l.FundedDate.Day == date).ToListAsync();
      return data;
    }


    
    
  }
}