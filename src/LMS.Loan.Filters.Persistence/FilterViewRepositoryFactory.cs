﻿using System;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Security.Encryption;
using LMS.Loan.Filters.Abstractions;

namespace LMS.Loan.Filters.Persistence {
    public class FilterViewRepositoryFactory : IFilterViewRepositoryFactory {
        public FilterViewRepositoryFactory (IServiceProvider provider) {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFilterViewRepository Create (ITokenReader reader) {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory> ();
            var tenantService = tenantServiceFactory.Create (reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory> ();

            var configuration = mongoConfigurationFactory.Get (reader, Provider.GetService<ILogger> ());;
            var encryptionService = Provider.GetService<IEncryptionService> ();

            return new FilterViewRepository (tenantService, configuration, encryptionService);
        }
    }
}