﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Loan.Filters.Abstractions.Configurations;
using LMS.Loan.Filters.Abstractions;
using System.Text;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
using Newtonsoft.Json;
using LendFoundry.StatusManagement;
using LendFoundry.ProductRule;
using LendFoundry.DataAttributes;
using LendFoundry.VerificationEngine;
using LMS.LoanManagement.Abstractions;
using Lendfoundry.CallManagement.Abstractions;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.ProductConfiguration;
using LendFoundry.ProductConfiguration.Client;

namespace LMS.Loan.Filters
{
    public class LoanFilterService : ILoanFilterServiceExtended
    {
        #region Constructor

        public LoanFilterService
        (
           ITagFilterRepository tagRepository,
            IFilterViewRepository repository,
            IEventHubClient eventHub,
            ILogger logger,
            ITenantTime tenantTime,
            Abstractions.Configurations.Configuration configuration,
            TaggingConfiguration taggingConfiguration,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            IProductRuleService productRuleService,
            IEntityStatusService statusManagementServiceClient,
            IDataAttributesEngine dataAttributesEngine,
            IVerificationEngineService verificationEngineService,
            ILoanOnboardingService loanService,
            ILoanMetricsRepository loanMetricsRepository,
            ICallManagementService callManagementService,
            IAchConfigurationService achService,
            IProductService productService
        )
        {
            TagsRepository = tagRepository;
            Repository = repository;
            EventHub = eventHub;
            Logger = logger;
            TenantTime = tenantTime;
            Configuration = configuration;
            TaggingConfiguration = taggingConfiguration;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            ProductRuleService = productRuleService;
            StatusManagementServiceClient = statusManagementServiceClient;
            DataAttributesEngine = dataAttributesEngine;
            VerificationEngineService = verificationEngineService;
            LoanService = loanService;
            LoanMetricsRepository = loanMetricsRepository;
            CallManagementService=callManagementService;
            AchService = achService;
            ProductService = productService;
        }

        #endregion

        #region Variables

        private ITagFilterRepository TagsRepository { get; }
        private Abstractions.Configurations.Configuration Configuration { get; }
        private ILoanOnboardingService LoanService { get; }
        private TaggingConfiguration TaggingConfiguration { get; }
        private IFilterViewRepository Repository { get; }
        private IEventHubClient EventHub { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }
        private ITokenHandler TokenParser { get; }
        private ITokenReader TokenReader { get; }
        private IProductRuleService ProductRuleService { get; }
        private IEntityStatusService StatusManagementServiceClient { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private IVerificationEngineService VerificationEngineService { get; }
        private ILoanMetricsRepository LoanMetricsRepository { get; }
        private ICallManagementService CallManagementService { get; }
        private IAchConfigurationService AchService { get; }
        private IProductService ProductService { get; }
        #endregion

        public IEnumerable<IFilterView> GetAll()
        {
            return Repository.GetAll();
        }

        public async Task<IFilterView> GetFilterView(string loanNumber)
        {
            return await Repository.GetFilterView(loanNumber);
        }

        public Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetAllByStatus(statuses);
        }

        public void ProcessTaggingEvent(EventInfo @event)
        {
            try
            {
                if (@event == null)
                    throw new InvalidArgumentException($"#{nameof(@event)} cannot be null", nameof(@event));

                var taggingEventConfiguration = TaggingConfiguration.Events.FirstOrDefault(x => x.Name == @event.Name);

                if (taggingEventConfiguration == null)
                    throw new ArgumentException($"Tagging event configuration for {@event.Name} not found");

                var loanNumber = taggingEventConfiguration.LoanNumber.FormatWith(@event);
                Logger.Info($"Processing tagging event {@event.Name} for application {loanNumber}");

                ApplyTags(loanNumber, taggingEventConfiguration.ApplyTags);
                RemoveTags(loanNumber, taggingEventConfiguration.RemoveTags);

                taggingEventConfiguration.Rules?.ToList().ForEach(async x =>
                {
                    var dataAttributes = await DataAttributesEngine.GetAllAttributes("loan", loanNumber);
                    // var verificationDetails = await VerificationEngineService.GetFactVerification("loan", loanNumber);
                    //var applicationStatusDetails = await StatusManagementServiceClient.GetActiveStatusWorkFlow("loan", loanNumber);
                    var input = new { Data = @event.Data, DataAttributes = dataAttributes };
                    var ruleName = x.Name;
                    var version = x.Version;

                    try
                    {
                        var loan = await LoanService.GetLoanInformationByLoanNumber(loanNumber);

                        ProductRuleResult ruleExecutionResult = null;
                        var result = await ProductRuleService.RunRule("loan", loanNumber, loan.LoanProductId, x.Name, input);

                        if (result != null)
                        {
                            ruleExecutionResult = result;
                            if (ruleExecutionResult.ExceptionDetail != null && ruleExecutionResult.ExceptionDetail.Count > 0)
                            {
                                throw new Exception("Unable to Run Rule TaggingRuleVerification");
                            }
                            if (ruleExecutionResult.IntermediateData != null)
                            {

                                var objTagResult = JsonConvert.DeserializeObject<TagResult>(ruleExecutionResult.IntermediateData.ToString());
                                if (objTagResult != null)
                                {
                                    Logger.Info($"{objTagResult.ApplyTags} to be applied to application {loanNumber}");
                                    Logger.Info($"{objTagResult.RemoveTags} to be removed from application {loanNumber}");

                                    ApplyTags(loanNumber, objTagResult.ApplyTags);
                                    RemoveTags(loanNumber, objTagResult.RemoveTags);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error($"Unhandled exception while processing application #{loanNumber} for rule #{ruleName}.", ex);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error($"The method ProcessTaggingEvent({@event}) raised an error:{ex.Message}, Full Error:", ex);
            }
        }

        public IFilterViewTags GetApplicationTagInformation(string loanNumber)
        {
            if (string.IsNullOrEmpty(loanNumber))
                throw new ArgumentNullException("Application Number required to fetch tag information");
            return TagsRepository.GetTagsForApplication(loanNumber);
        }

        public async Task<ILoanMetrics> GetMetrics(string loanNumber)
        {
            if (string.IsNullOrEmpty(loanNumber))
                throw new ArgumentNullException("Application Number required to fetch tag information");
            return await LoanMetricsRepository.GetMetrics(loanNumber);
        }

        public async Task<List<CollectionResponse>> GetCollectionData(LoanCollectionRequest loanCollectionRequest)
        {
            if (loanCollectionRequest == null)
                throw new ArgumentNullException($"{nameof(loanCollectionRequest)} can not be null");
                
            if (loanCollectionRequest.ToDate < loanCollectionRequest.FromDate)
                throw new ArgumentException ("To date should be less then from date");
            loanCollectionRequest.ToDate = loanCollectionRequest.ToDate.AddDays(1);
            var loans =  await Repository.GetCollectionData(loanCollectionRequest);
            var CollectionData =new List<CollectionResponse>();

            var funders = AchService.GetFundingSources();
            var products = await ProductService.GetAll();

            foreach (var loan in loans)
            {
                try
                {
                    for (int i = 0; i < loan.CollectionDetails.Count; i++)
                    {
                    var data = new CollectionResponse();
                    data.LoanNumber = loan.LoanNumber;
                    data.LoanProductId = loan.LoanProductId;
                    data.StatusWorkFlowId = loan.StatusWorkFlowId;
                    data.ProductPortfolioType =loan.ProductPortfolioType;
                    data.BorrowerName = loan.PrimaryApplicantFirstName + " "+ loan.PrimaryApplicantLastName;
                    data.RepaymentAmount = loan.ProductPortfolioType.ToString() == ProductPortfolioType.MCA.ToString() || loan.ProductPortfolioType.ToString() == ProductPortfolioType.MCALOC.ToString() ? Math.Round(loan.LoanAmount * loan.FactorRate ,2) : 0;
                    data.PreviousCollectionStage =string.IsNullOrWhiteSpace(loan.CollectionDetails[i].OldCollectionStage) ? "NA": loan.CollectionDetails[i].OldCollectionStage;
                    data.CurrentCollectionStage = loan.CollectionDetails[i].CurrentCollectionStage;
                    data.DateOfTransition = loan.CollectionDetails[i].DateOfTransition;
                    data.ReasonOfTransition =string.IsNullOrWhiteSpace(loan.CollectionDetails[i].OldCollectionStage) || loan.CollectionDetails[i].OldCollectionStage == null? loan.CollectionDetails[i].ReasonOfTransition :  loan.CollectionDetails[i].ReasonOfTransition +( loan.CollectionDetails[i].ReasonOfTransition == "1" ? " Miss Payment" :" Miss Payments");
                    data.TotalNumberOfPaymentMade = loan.NumberOfPaymentMade;
                    data.TotalAmountReceived = loan.PaymentInfo!=null ?Math.Round( loan.PaymentInfo.CumulativeTotalPaid,2) : 0;
                    data.TotalOutstandingAmount =  loan.PBOT != null ? Math.Round(loan.PBOT.TotalPrincipalOutStanding + loan.PBOT.TotalInterestOutstanding + loan.PBOT.TotalFeeOutstanding,2) : 0;
                    data.TotalOutstandingPercentage =  data.RepaymentAmount > 0 ?Math.Round( data.TotalOutstandingAmount / data.RepaymentAmount,2) : 0;
                    if(!string.IsNullOrEmpty(loan.FunderId) && funders != null && funders.Count > 0){
                        data.FunderId = loan.FunderId;
                        var filteredFunder =  funders.FirstOrDefault(x => x.Id == data.FunderId);
                        if(filteredFunder != null){
                            data.FunderName = filteredFunder.Name;
                        }
                    }
                    if(!string.IsNullOrEmpty(loan.LoanProductId) && products != null && products.Count > 0){
                        var filteredProduct =  products.FirstOrDefault(x => x.ProductId == data.LoanProductId);
                        if(filteredProduct != null){
                            data.ProductName = filteredProduct.Name;
                        }
                    }
                    CollectionData.Add(data);
                    }  
                }
                catch (System.Exception e)
                {
                    Logger.Error($"Error for loan in GetCollectionData {loan.LoanNumber} : " , e.Message);
                }            
            }
           return CollectionData;
        }

  public async Task<List<CollectionResponse>> GetFundingData(LoanCollectionRequest loanCollectionRequest)
        {
            if (loanCollectionRequest == null)
                throw new ArgumentNullException($"{nameof(loanCollectionRequest)} can not be null");
                
            if (loanCollectionRequest.ToDate < loanCollectionRequest.FromDate)
                throw new ArgumentException ("To date should be less then from date");
            loanCollectionRequest.ToDate = loanCollectionRequest.ToDate.AddDays(1);
            var loans =  await Repository.GetFundingData(loanCollectionRequest);
            var CollectionData =new List<CollectionResponse>();

            var funders = AchService.GetFundingSources();
            var products = await ProductService.GetAll();

            if(loans != null)
            {
            foreach (var loan in loans)
            {
                try
                {
                   
                    var data = new CollectionResponse();
                    data.LoanNumber = loan.LoanNumber;
                    
                    data.LoanProductId = loan.LoanProductId;
                    data.StatusWorkFlowId = loan.StatusWorkFlowId;
                    data.ProductPortfolioType =loan.ProductPortfolioType;
                    data.BorrowerName = loan.PrimaryApplicantFirstName + " "+ loan.PrimaryApplicantLastName;
                    data.RepaymentAmount = loan.ProductPortfolioType.ToString() == ProductPortfolioType.MCA.ToString() || loan.ProductPortfolioType.ToString() == ProductPortfolioType.MCALOC.ToString() ? Math.Round(loan.LoanAmount * loan.FactorRate ,2) : 0;
                    data.TotalNumberOfPaymentMade = loan.NumberOfPaymentMade;
                    data.TotalAmountReceived = loan.PaymentInfo!=null ?Math.Round( loan.PaymentInfo.CumulativeTotalPaid,2) : 0;
                    data.TotalOutstandingAmount =  loan.PBOT != null ? Math.Round(loan.PBOT.TotalPrincipalOutStanding + loan.PBOT.TotalInterestOutstanding + loan.PBOT.TotalFeeOutstanding,2) : 0;
                    data.TotalOutstandingPercentage =  data.RepaymentAmount > 0 ?Math.Round( data.TotalOutstandingAmount / data.RepaymentAmount,2) : 0;
                    if(!string.IsNullOrEmpty(loan.FunderId) && funders != null && funders.Count > 0){
                        data.FunderId = loan.FunderId;
                        var filteredFunder =  funders.FirstOrDefault(x => x.Id == data.FunderId);
                        if(filteredFunder != null){
                            data.FunderName = filteredFunder.Name;
                        }
                    }
                    if(!string.IsNullOrEmpty(loan.LoanProductId) && products != null && products.Count > 0){
                        var filteredProduct =  products.FirstOrDefault(x => x.ProductId == data.LoanProductId);
                        if(filteredProduct != null){
                            data.ProductName = filteredProduct.Name;
                        }
                    }
                    data.FundedAmount = Math.Round(loan.Fundedamount,2);
                    data.LoanStatus = loan.StatusName;
                    CollectionData.Add(data);
                      
                }
                catch (System.Exception e)
                {
                    Logger.Error($"Error for loan in GetFundingData {loan.LoanNumber} : " , e.Message);
                }    
            }        
            }
           return CollectionData;
        }
        public async Task<List<CollectionResponse>> GetFollowUpData(LoanCollectionRequest loanCollectionRequest)
        {
            if (loanCollectionRequest == null)
                throw new ArgumentNullException($"{nameof(loanCollectionRequest)} can not be null");
                
            if (loanCollectionRequest.ToDate < loanCollectionRequest.FromDate)
                throw new ArgumentException ("To date should be less then from date");

            var funders = AchService.GetFundingSources();
            var products = await ProductService.GetAll();

            loanCollectionRequest.ToDate = loanCollectionRequest.ToDate.AddDays(1);
            var loans = await CallManagementService.GetFollowUpReport(new SearchParam() {FollowUpFromDate = loanCollectionRequest.FromDate.Date , FollowUpToDate =  loanCollectionRequest.ToDate.Date});
            loans = loans.Where(x=>x.EntityType == "loan").OrderByDescending(x=>x.FollowUpDate).ToList();
            if(loanCollectionRequest.LastFromDate != null && loanCollectionRequest.LastToDate != null)
            {
                if (loanCollectionRequest.LastToDate <loanCollectionRequest.LastFromDate)
                    throw new ArgumentException ("Last Call date To date should be less then from date");
                loans = loans.Where(x =>x.CallDateTime.Date >=  loanCollectionRequest.LastFromDate.Value.Date && x.CallDateTime.Date <= loanCollectionRequest.LastToDate.Value.Date).ToList();    
            }
            if(!string.IsNullOrWhiteSpace(loanCollectionRequest.LastCallDesposition))
            {
                List<string> dispositions = loanCollectionRequest.LastCallDesposition.Split(',').ToList();
                loans = loans.Where(x => dispositions.Contains(x.CallDisposition)).ToList();    

            }
           
            var CollectionData =new List<CollectionResponse>();
            List<string> loanIds = loans.Select(x =>x.EntityId).ToList();
            var filterData = await Repository.GetLoansByLoanNumbers(loanIds);

            foreach (var loan in loans)
            {
             try{    
                var loanData = filterData.FirstOrDefault(x=>x.LoanNumber == loan.EntityId);
                var data = new CollectionResponse();
                data.LoanNumber = loan.EntityId;
                data.LoanProductId = loanData.LoanProductId;
                data.StatusWorkFlowId = loanData.StatusWorkFlowId;
                data.ProductPortfolioType =loanData.ProductPortfolioType;
                data.BorrowerName = loanData.PrimaryApplicantFirstName + " "+ loanData.PrimaryApplicantLastName;
                data.RepaymentAmount = loanData.ProductPortfolioType.ToString() == ProductPortfolioType.MCA.ToString() || loanData.ProductPortfolioType.ToString() == ProductPortfolioType.MCALOC.ToString() ? Math.Round(loanData.LoanAmount * loanData.FactorRate ,2) : 0;               
                data.FundedAmount = loanData.Fundedamount;
                data.PrimaryPhoneNumber = loanData.PrimaryApplicantPhoneNo;
                data.SecondaryPhoneNumber = loanData.SecondaryApplicantPhoneNo;
                data.LoanStatus = loanData.StatusName;
                data.LastCallDate =new TimeBucket(loan.CallDateTime);
                data.LastCallDisposition = loan.CallDisposition;
                data.LastSuccessfulContactedDate =loan.CallDisposition.ToLower() == "contacted" ? data.LastCallDate : null;
                data.FollowUpDate = new TimeBucket(loan.FollowUpDate);
                data.TotalAmountReceived = loanData.PaymentInfo!=null ?Math.Round( loanData.PaymentInfo.CumulativeTotalPaid,2) : 0;
                data.TotalOutstandingAmount =  loanData.PBOT != null ? Math.Round(loanData.PBOT.TotalPrincipalOutStanding + loanData.PBOT.TotalInterestOutstanding + loanData.PBOT.TotalFeeOutstanding,2) : 0;
                data.TotalOutstandingPercentage =  data.RepaymentAmount > 0 ?Math.Round( data.TotalOutstandingAmount / data.RepaymentAmount,2) : 0;
                data.BorrowerId=loanData.BorrowerId;
                data.BusinessMaxLimitAmount=loanData.BusinessMaxLimitAmount;
                if(!string.IsNullOrEmpty(loanData.FunderId) && funders != null && funders.Count > 0){
                    data.FunderId = loanData.FunderId;
                    var filteredFunder =  funders.FirstOrDefault(x => x.Id == data.FunderId);
                    if(filteredFunder != null){
                        data.FunderName = filteredFunder.Name;
                    }
                }
                if(!string.IsNullOrEmpty(loanData.LoanProductId) && products != null && products.Count > 0){
                    var filteredProduct =  products.FirstOrDefault(x => x.ProductId == data.LoanProductId);
                    if(filteredProduct != null){
                        data.ProductName = filteredProduct.Name;
                    }
                }
                CollectionData.Add(data);
               
                }
                catch (System.Exception e)
                {
                    Logger.Error($"Error for loan in GetFollowUpData {loan.EntityId} : " , e.Message);
                }            
            }
            return CollectionData;
        }
  
        public async Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any())
                throw new ArgumentException($"{nameof(tags)} is mandatory");

            IEnumerable<IFilterView> result = null;
            var taggedApplications = await TagsRepository.GetByTags(tags);

            if (taggedApplications != null)
            {
                var loanNumbers = taggedApplications.Select(x => x.LoanNumber).ToList();
                result = await Repository.All(x => loanNumbers.Contains(x.LoanNumber));
            }

            return result;
        }

        public Task<int> GetCountByTag(IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any())
                throw new ArgumentException($"{nameof(tags)} is mandatory");

            return TagsRepository.GetCountByTag(tags);
        }

        public async Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag)
        {
            return await TagsRepository.GetTagInfoForApplicationsByTag(tag);
        }

        public void TagsWithoutEvaluation(string loanNumber, IEnumerable<string> tags)
        {
            if (string.IsNullOrEmpty(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} is mandatory", nameof(loanNumber));
            if (tags == null || tags.Count() == 0)
                throw new ArgumentNullException($"{nameof(tags)} is mandatory", nameof(tags));

            ApplyTags(loanNumber, tags);
        }

        public void UnTagWithoutEvaluation(string loanNumber, IEnumerable<string> tags)
        {
            if (string.IsNullOrEmpty(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} is mandatory", nameof(loanNumber));
            if (tags == null || tags.Count() == 0)
                throw new ArgumentNullException($"{nameof(tags)} is mandatory", nameof(tags));

            RemoveTags(loanNumber, tags);
        }

        public async Task<List<ILoanMetrics>> GetAllMetrics()
        {
            return await LoanMetricsRepository.GetAllMetrics();
        }

        public Task<IEnumerable<IFilterView>> GetLoansByProductId(string productid)
        {
            var filters = Repository.GetLoansByProductId(productid);
            return filters;
        }

        public async Task<List<IFilterView>> GetLoansByLoanNumbers(List<string> loanNumbers)
        {
            if (loanNumbers == null)
                throw new ArgumentException("Argument is null ", nameof(loanNumbers));
            return await Repository.GetLoansByLoanNumbers(loanNumbers);
        }

        #region Private methods

        private void ApplyTags(string loanNumber, IEnumerable<string> tags)
        {
            tags?.ToList().ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x))
                {
                    TagsRepository.AddTag(loanNumber, x);
                }
            });
        }

        private void RemoveTags(string loanNumber, IEnumerable<string> tags)
        {
            tags?.ToList().ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x))
                {
                    if (x == "ALL")
                    {
                        RemoveAllTags(loanNumber);
                        return;
                    }
                    TagsRepository.RemoveTag(loanNumber, x);
                }
            });
        }

        private void RemoveAllTags(string loanNumber)
        {
            TagsRepository.ClearAllTags(loanNumber);
        }

        #endregion

        public async Task<List<IFilterView>> GetByBorrowerId(string borrowerId)
        {
            if (string.IsNullOrWhiteSpace(borrowerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(borrowerId));
            return await Repository.GetByBorrowerId(borrowerId);
        }

        public async Task<IEnumerable<IFilterView>> GetLoansByOnBoardedByDate(int year, int month, int day)
        {
            return await Repository.GetLoansByOnBoardedByDate(year,month,day);
        }

    }
}