﻿using LMS.Loan.Filters.Abstractions;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LMS.Loan.Filters
{
    public static class LoanFilterListenerExtensions
    {
        public static void UseLoanFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<ILoanFilterListener>().Start();
        }
    }
}
