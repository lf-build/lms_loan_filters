﻿using LMS.Loan.Filters.Abstractions;
using System;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LMS.Loan.Filters.Abstractions.Configurations;
using LendFoundry.ProductRule.Client;
using LendFoundry.StatusManagement.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.VerificationEngine.Client;
using LMS.LoanManagement.Client;
using LendFoundry.CallManagement.Client;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.ProductConfiguration;
using LendFoundry.ProductConfiguration.Client;

namespace LMS.Loan.Filters
{
    public class LoanFilterServiceFactory : ILoanFilterServiceFactory
    {
        public LoanFilterServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public ILoanFilterServiceExtended Create(ITokenReader reader, ILogger logger, ITokenHandler handler)
        {
            var repositoryFactory = Provider.GetService<IFilterViewRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            var tagRepositoryFactory = Provider.GetService<ITagFilterRepositoryFactory>();
            var tagRepository = tagRepositoryFactory.Create(reader);

            var matricsRepositoryFactory = Provider.GetService<ILoanMetricsRepositoryFactory>();
            var matricsRepository = matricsRepositoryFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);
            
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var taggingConfigurationService = configurationServiceFactory.Create<TaggingConfiguration>(Settings.TaggingEvents, reader);
            var taggingConfiguration = taggingConfigurationService.Get();

            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);
            var productRuleFactory = Provider.GetService<IProductRuleServiceClientFactory>();
            var productRuleEngine = productRuleFactory.Create(reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusService = statusManagementServiceFactory.Create(reader);

            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesEngineService = dataAttributesClientFactory.Create(reader);

            var verficationEngineServiceFactory = Provider.GetService<IVerificationEngineServiceClientFactory>();
            var verificationEngineService = verficationEngineServiceFactory.Create(reader);

            var scheduleServiceFactory = Provider.GetService<ILoanOnboardingClientServiceFactory>();
            var scheduleService = scheduleServiceFactory.Create(reader);

            var callManagementServiceFactory = Provider.GetService<ICallManagementServiceFactory>();
            var callService = callManagementServiceFactory.Create(reader);

            var achServiceFactory = Provider.GetService<IAchConfigurationServiceFactory>();
            var achService = achServiceFactory.Create(reader);

            var productService = Provider.GetService<IProductServiceFactory>();
            var product = productService.Create(reader);

            return new LoanFilterService(tagRepository, repository, eventHub, logger, tenantTime, configuration, taggingConfiguration, reader, handler, productRuleEngine, statusService, dataAttributesEngineService, verificationEngineService, scheduleService, matricsRepository, callService, achService, product);
        }
    }
}
