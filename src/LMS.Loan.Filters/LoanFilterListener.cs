﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using LMS.Loan.Filters.Abstractions;
using LMS.Loan.Filters.Abstractions.Configurations;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.LoanManagement.Client;
using LMS.Payment.Processor;
using LMS.Payment.Processor.Client;
using Newtonsoft.Json;
using Configuration = LMS.Loan.Filters.Abstractions.Configurations.Configuration;

namespace LMS.Loan.Filters {
    public class LoanFilterListener : ILoanFilterListener {
        public LoanFilterListener
            (
                IConfigurationServiceFactory<Configuration> apiConfigurationFactory,
                ITokenHandler tokenHandler,
                IEventHubClientFactory eventHubFactory,
                IFilterViewRepositoryFactory repositoryFactory,
                ILoggerFactory loggerFactory,
                ITenantServiceFactory tenantServiceFactory,
                IConfigurationServiceFactory configurationFactory,
                ITenantTimeFactory tenantTimeFactory,
                IConfigurationServiceFactory<TaggingConfiguration> apiTaggingConfigurationFactory,
                ILoanFilterServiceFactory loanFilterServiceFactory,
                ILoanOnboardingClientServiceFactory loanOnboardingClientServiceFactory,
                ILoanScheduleClientServiceFactory loanScheduleClientServiceFactory,
                IStatusManagementServiceFactory statusManagementFactory,
                IAccrualBalanceClientFactory accrualBalanceServiceFactory,
                IPaymentServiceClientFactory paymentServiceClientFactory,
                ILoanMetricsRepositoryFactory loanMetricsRepositoryFactory
            ) {
                EventHubFactory = eventHubFactory;
                ApiConfigurationFactory = apiConfigurationFactory;
                TokenHandler = tokenHandler;
                RepositoryFactory = repositoryFactory;
                LoggerFactory = loggerFactory;
                TenantServiceFactory = tenantServiceFactory;
                TenantTimeFactory = tenantTimeFactory;
                ConfigurationFactory = configurationFactory;
                ApiTaggingConfigurationFactory = apiTaggingConfigurationFactory;
                LoanFilterServiceFactory = loanFilterServiceFactory;
                LoanOnboardingClientServiceFactory = loanOnboardingClientServiceFactory;
                LoanScheduleClientServiceFactory = loanScheduleClientServiceFactory;
                StatusManagementFactory = statusManagementFactory;
                AccrualBalanceServiceFactory = accrualBalanceServiceFactory;
                PaymentServiceClientFactory = paymentServiceClientFactory;
                LoanMetricsRepositoryFactory = loanMetricsRepositoryFactory;
            }

        #region Variables

        private IStatusManagementServiceFactory StatusManagementFactory { get; }
        private ILoanScheduleClientServiceFactory LoanScheduleClientServiceFactory { get; set; }
        private ILoanOnboardingClientServiceFactory LoanOnboardingClientServiceFactory { get; }
        private IConfigurationServiceFactory<Configuration> ApiConfigurationFactory { get; }
        private IConfigurationServiceFactory<TaggingConfiguration> ApiTaggingConfigurationFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IFilterViewRepositoryFactory RepositoryFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ILoanFilterServiceFactory LoanFilterServiceFactory { get; }
        private IAccrualBalanceClientFactory AccrualBalanceServiceFactory { get; }
        private IPaymentServiceClientFactory PaymentServiceClientFactory { get; }
        private ILoanMetricsRepositoryFactory LoanMetricsRepositoryFactory { get; }

        private static Object Key = new object ();

        #endregion

        public void Start () {
            var logger = LoggerFactory.Create (NullLogContext.Instance);
            logger.Info ($"Eventhub listener started");

            try {
                var emptyReader = new StaticTokenReader (string.Empty);

                var tenantService = TenantServiceFactory.Create (emptyReader);
                var tenants = tenantService.GetActiveTenants ();
                logger.Info ($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach (tenant => {

                    logger.Info ($"Processing for tenant #{tenant.Id}");
                    try{
                        // Tenant token creation
                        var token = TokenHandler.Issue (tenant.Id, "loan-filters");
                        var reader = new StaticTokenReader (token.Value);
                        var hub = EventHubFactory.Create (reader);
                        // Needed resources for this operation
                        var repository = RepositoryFactory.Create (reader);
                        var configuration = ApiConfigurationFactory.Create (reader).Get ();
                        if (configuration != null && configuration.Events != null){
                        var tenantTime = TenantTimeFactory.Create (ConfigurationFactory, reader);
                        var taggingConfiguration = ApiTaggingConfigurationFactory.Create (reader).Get ();
                        var loanService = LoanOnboardingClientServiceFactory.Create (reader);
                        var loanSchedule = LoanScheduleClientServiceFactory.Create (reader);
                        var statusManagementService = StatusManagementFactory.Create (reader);
                        var accrualService = AccrualBalanceServiceFactory.Create (reader);
                        var paymentProcessorService = PaymentServiceClientFactory.Create (reader);
                        var loanFilterService = LoanFilterServiceFactory.Create (reader, logger, TokenHandler);
                        var loanMetricsRepository = LoanMetricsRepositoryFactory.Create (reader);
                        // Attach all configured events to be listen
                        configuration
                            .Events
                            .ToList ()
                            .ForEach (eventConfig => {
                                hub.On (eventConfig.Name, AddView (eventConfig, repository,
                                    logger, tenantTime, taggingConfiguration, hub, loanFilterService, configuration, loanService, loanSchedule, statusManagementService, accrualService, paymentProcessorService, loanMetricsRepository));
                                logger.Info ($"Event #{eventConfig.Name} attached to listener for tenant {tenant.Id}");
                            });
                        }
                    } catch(Exception e){
                        logger.Error ($"Error while processing tenant #{tenant.Id}", e);
                    }
                });
            } catch (Exception ex) {
                logger.Error ("Error while listening eventhub", ex);
                // Start();
            }
        }

        private static Action<EventInfo> AddView (
            Abstractions.Configurations.EventMapping eventConfiguration,
            IFilterViewRepository repository,
            ILogger logger,
            ITenantTime tenantTime,
            TaggingConfiguration taggingConfiguration,
            IEventHubClient eventHubService,
            ILoanFilterServiceExtended loanFilterService,
            Configuration configuration,
            ILoanOnboardingService loanService,
            ILoanScheduleService loanschduleService,
            IEntityStatusService statusManagementService,
            IAccrualBalanceService AccrualBalanceService,
            IPaymentProcessorService paymentProcessorService,
            ILoanMetricsRepository loanMetricsRepository
        ) {
            return @event => {
                try {
                    var loanNumber = eventConfiguration.LoanNumber.FormatWith (@event);

                    if (eventConfiguration.ShouldUpdateAppFilterRecord == true) {

                        logger.Info ($"Now processing {@event.Name} with payload {@event.Data}");

                        logger.Info ($"Loan Number : #{loanNumber}");

                        var loan = loanService.GetLoanInformationByLoanNumber (loanNumber).Result;

                        // snapshot creation
                        var data = new FilterView ();
                        data.LoanApplicationNumber = loan.LoanApplicationNumber;
                        data.LoanNumber = loan.LoanNumber;
                        data.LoanOnboardedDate = loan.LoanOnboardedDate;
                        data.LoanEndDate = loan.LoanEndDate;
                        data.LoanFundedDate = loan.LoanFundedDate;
                        data.LoanProductId = loan.LoanProductId;
                        data.LoanStartDate = loan.LoanStartDate;
                        data.ProductPortfolioType = loan.ProductPortfolioType.ToString ();
                        data.ProductCategory = loan.ProductCategory.ToString ();
                        data.IsAutoPay = loan.IsAutoPay;
                        data.AutoPayStartDate = loan.AutoPayStartDate;
                        data.AutoPayStopDate = loan.AutoPayStopDate;
                        data.BorrowerId = loan.BorrowerId;
                        data.FunderId = loan.FunderId;
                        data.AggregatorName = loan.AggregatorName;
                        data.Beneficiary = loan.Beneficiary;
                        data.AdditionalRefId = loan.AdditionalRefId;

                        if (loan.PrimaryApplicantDetails != null) {
                            if (loan.PrimaryApplicantDetails.Addresses != null && loan.PrimaryApplicantDetails.Addresses.Count > 0) {
                                data.PrimaryApplicantAddressLine1 = loan.PrimaryApplicantDetails.Addresses.FirstOrDefault ().AddressLine1;
                                data.PrimaryApplicantAddressLine2 = loan.PrimaryApplicantDetails.Addresses.FirstOrDefault ().AddressLine2;
                                data.PrimaryApplicantCity = loan.PrimaryApplicantDetails.Addresses.FirstOrDefault ().City;
                                data.PrimaryApplicantState = loan.PrimaryApplicantDetails.Addresses.FirstOrDefault ().State;
                                data.PrimaryApplicantZip = loan.PrimaryApplicantDetails.Addresses.FirstOrDefault ().Zip;
                            }
                            if (loan.PrimaryApplicantDetails.Name != null) {
                                data.PrimaryApplicantFirstName = loan.PrimaryApplicantDetails.Name.First;
                                data.PrimaryApplicantGeneration = loan.PrimaryApplicantDetails.Name.Generation;
                                data.PrimaryApplicantLastName = loan.PrimaryApplicantDetails.Name.Last;
                                data.PrimaryApplicantMiddle = loan.PrimaryApplicantDetails.Name.Middle;
                                data.PrimaryApplicantSalutation = loan.PrimaryApplicantDetails.Name.Salutation;
                            }
                            if (loan.PrimaryApplicantDetails.Emails != null && loan.PrimaryApplicantDetails.Emails.Count > 0) {
                                data.PrimaryApplicantEmailAddress = loan.PrimaryApplicantDetails.Emails.FirstOrDefault ().EmailAddress;
                            }
                            if (loan.PrimaryApplicantDetails.Phones != null && loan.PrimaryApplicantDetails.Phones.Count > 0) {
                                data.PrimaryApplicantPhoneNo = loan.PrimaryApplicantDetails.Phones.FirstOrDefault ().PhoneNo;
                                data.SecondaryApplicantPhoneNo = loan.PrimaryApplicantDetails.Phones.Count > 1 ? loan.PrimaryApplicantDetails.Phones[1].PhoneNo : string.Empty;
                            }
                            data.PrimaryApplicantDOB = loan.PrimaryApplicantDetails.PII != null ? loan.PrimaryApplicantDetails.PII.DOB.ToString () : string.Empty;
                            data.PrimaryApplicantSSN = loan.PrimaryApplicantDetails.PII?.SSN;
                            data.PrimaryApplicantPositionOfLoan = loan.PrimaryApplicantDetails.PositionOfLoan;
                            data.PrimaryApplicantRole = loan.PrimaryApplicantDetails.Role;
                            data.Personalidentifiers=new Dictionary<string,string>();
                            if (loan.PrimaryApplicantDetails.PII?.PersonalIdentifiers != null && loan.PrimaryApplicantDetails.PII?.PersonalIdentifiers.Count > 0)
                            {
                                foreach (var item in loan.PrimaryApplicantDetails.PII?.PersonalIdentifiers) {
                                    data.Personalidentifiers.Add (item.Type, item.Id);
                                }
                            }
                        }
                        if (loan.PrimaryBusinessDetails != null) {
                            if (loan.PrimaryBusinessDetails.Address != null) {
                                data.PrimaryBusinessAddressLine1 = loan.PrimaryBusinessDetails.Address.AddressLine1;
                                data.PrimaryBusinessAddressLine2 = loan.PrimaryBusinessDetails.Address.AddressLine2;
                                data.PrimaryBusinessCity = loan.PrimaryBusinessDetails.Address.City;
                                data.PrimaryBusinessState = loan.PrimaryBusinessDetails.Address.State;
                                data.PrimaryBusinessZip = loan.PrimaryBusinessDetails.Address.Zip;
                            }

                            data.PrimaryBusinessEmailAddress = loan.PrimaryBusinessDetails.Email?.EmailAddress;
                            data.PrimaryBusinessPhoneNo = loan.PrimaryBusinessDetails.Phone?.PhoneNo;
                            data.PrimaryBusinessFedTaxId = loan.PrimaryBusinessDetails.FedTaxId;
                            data.PrimaryBusinessDBA = loan.PrimaryBusinessDetails.DBA;
                            data.PrimaryBusinessName = loan.PrimaryBusinessDetails.BusinessName;
                            data.BusinessIdentifiers =new Dictionary<string,string>();
                            if (loan.PrimaryBusinessDetails?.BusinessIdentifiers != null && loan.PrimaryBusinessDetails?.BusinessIdentifiers.Count > 0) {
                                foreach (var item in loan.PrimaryBusinessDetails?.BusinessIdentifiers) {
                                    data.BusinessIdentifiers.Add (item.Type, item.Id);
                                }
                            }
                            data.BusinessMaxLimitAmount=loan.PrimaryBusinessDetails.BusinessMaxLimitAmount;
                        }

                        var schduleDetails = loanschduleService.GetLoanSchedule(loan.LoanNumber).Result;
                        if (schduleDetails != null) {
                            data.CurrentLoanScheduleNumber = schduleDetails.LoanScheduleNumber;
                            data.CurrentLoanScheduleVersion = schduleDetails.ScheduleVersionNo;
                            data.LoanAmount = schduleDetails.LoanAmount;
                            data.Fundedamount = schduleDetails.FundedAmount;
                            data.FundedDate = schduleDetails.FundedDate;
                            data.FirstPaymentDate = schduleDetails.FirstPaymentDate;
                            data.PaymentFrequency = schduleDetails.PaymentFrequency.ToString ();
                            data.DailyRate = schduleDetails.DailyRate;
                            data.AnnualRate = schduleDetails.AnnualRate;
                            data.Tenure = schduleDetails.Tenure;
                            data.FactorRate = schduleDetails.FactorRate;
                            data.CreditLimit = schduleDetails.CreditLimit;
                            data.LoanModReason = schduleDetails.ModReason;
                        }

                        var accrualDetails = AccrualBalanceService.GetLoanByScheduleVersion (loanNumber, schduleDetails.ScheduleVersionNo).Result;
                        var scheduleTrekkingInfo = AccrualBalanceService.GetScheduleByLoanNumberVersion (loan.LoanNumber, schduleDetails.ScheduleVersionNo).Result;

                        if (accrualDetails != null) {
                            data.PrincipalAmount = accrualDetails.PrincipalAmount;
                            data.AccrualStop = accrualDetails.AccrualStop;
                            data.AccrualStopDate = new TimeBucket (accrualDetails.AccrualStopDate);
                            data.ProcessingDate = new TimeBucket (accrualDetails.ProcessingDate);
                            data.PBOC = accrualDetails.PBOC;
                            data.PBOT = accrualDetails.PBOT;
                            data.PaymentInfo = accrualDetails.PaymentInfo;
                            data.Due = accrualDetails.Due;
                            data.ChargeOff = accrualDetails.ChargeOff;
                            data.SellOff = accrualDetails.SellOff;
                            data.PayOff = accrualDetails.PayOff;
                            data.ExcessMoney = accrualDetails.ExcessMoney;
                            data.PriorInterest = accrualDetails.PriorInterestOutStanding;
                        }
                        data.NumberOfPaymentMade = scheduleTrekkingInfo != null && scheduleTrekkingInfo.ScheduleTracking != null ? scheduleTrekkingInfo.ScheduleTracking.Where (x => x.IsPaid == true).Count () : 0;
                        var statusDetails = statusManagementService.GetActiveStatusWorkFlow ("loan", loan.LoanNumber).Result;
                        if (statusDetails != null) {
                            data.StatusCode = statusDetails.Code;
                            data.StatusDate = new TimeBucket (statusDetails.ActiveOn.Time);
                            data.Reasons = statusDetails.ReasonsSelected;
                            data.StatusName = statusDetails.Name;
                            data.SubStatusDetail = statusDetails.SubStatusDetail;
                            data.StatusWorkFlowId = statusDetails.StatusWorkFlowId;
                            data.WorkFlowStatus = statusDetails.WorkFlowStatus;
                        }

                        if (eventConfiguration.ShouldUpdateFlagCount) {
                            try {
                                var flagCount = eventConfiguration.Response.FormatWith (@event);
                                data.NoOfActiveFlgas = flagCount != null ? Convert.ToInt16 (flagCount) : 0;
                            } catch (System.Exception) { }
                        }

                        try {
                            var filterView = loanFilterService.GetFilterView (loanNumber).Result;
                            data.CollectionDetails = filterView != null && filterView.CollectionDetails != null && filterView.CollectionDetails.Count > 0 ? filterView.CollectionDetails : new List<CollectionDetails> ();
                            //Collection Details
                            if (schduleDetails.ModStatus == ModStatus.Active && eventConfiguration.ShouldUpdateCollectionRecord) {

                                var oldCollectionStage = eventConfiguration.Response.FormatWith (@event);
                                var currentCollectionDetails = new CollectionDetails ();
                                currentCollectionDetails.CurrentCollectionStage = loan.CollectionStage;
                                currentCollectionDetails.DateOfTransition = new TimeBucket (tenantTime.Now.Date);
                                currentCollectionDetails.OldCollectionStage = oldCollectionStage;
                                currentCollectionDetails.ReasonOfTransition = string.IsNullOrWhiteSpace (currentCollectionDetails.OldCollectionStage) || currentCollectionDetails.OldCollectionStage == null ? "Payment schedule modified" : schduleDetails.PaymentFailure.ToString ();
                                data.CollectionDetails.Add (currentCollectionDetails);

                                logger.Info ($"CollectionDetails added to loan {loanNumber} based on event {@event.Name}");
                            }
                            var payments = paymentProcessorService.GetFundingData ("loan", loanNumber).Result;
                            var requestStatus = new List<string> { "failed", "initiated", "noa", "resubmitted", "submitted" };
                            var filteredFundingData = payments.Where (p => requestStatus.Contains (p.RequestStatus));

                            data.PaymentDetails = new List<IPaymentDetails> ();
                            foreach (var payment in filteredFundingData) {
                                ILoanPaymentAttempts attemptToBeUpdated = null;
                                if (payment.RequestStatus == "failed" || payment.RequestStatus == "noa") {
                                    var attempts = paymentProcessorService.GetAttemptData (payment.ReferenceId).Result;
                                    attemptToBeUpdated = attempts != null ? attempts.OrderByDescending (c => c.AttemptDate.Time.DateTime).FirstOrDefault () : null;
                                }

                                if (payment != null) {
                                    var paymentDetail = new PaymentDetails ();
                                    paymentDetail.ReferenceId = payment.ReferenceId;
                                    paymentDetail.ActivityDate = payment.RequestedDate;
                                    paymentDetail.ActivityStatus = payment.RequestStatus;
                                    paymentDetail.PaymentAmount = payment.PaymentAmount;
                                    paymentDetail.ScheduleDate = payment.PaymentScheduleDate;
                                    if (attemptToBeUpdated != null) {
                                        paymentDetail.ReturnCode = attemptToBeUpdated.ReturnCode;
                                        paymentDetail.ReturnDate = attemptToBeUpdated.ReturnDate;
                                        paymentDetail.ReturnReason = attemptToBeUpdated.ReturnReason;
                                    }
                                    data.PaymentDetails.Add (paymentDetail);
                                }
                            }
                        } catch {
                            logger.Error ($"Error in adding payment/Collcetion details for loan {loanNumber} based on event {@event.Name}");
                        }

                        lock (Key) {
                            repository.AddOrUpdate (data);
                        }

                        logger.Info ($"New snapshot added to loan {loanNumber} based on event {@event.Name}");

                    }
                    if (taggingConfiguration.Events.Exists (x => x.Name.ToLower () == @event.Name.ToLower ())) {
                        loanFilterService.ProcessTaggingEvent (@event);
                    }
                    if (configuration.MetricsEvent.Exists (x => x.Name.ToLower () == @event.Name.ToLower ())) {
                        UpdateMetrics (configuration, loanMetricsRepository, @event, loanNumber, eventHubService).Wait();
                    }
                } catch (Exception ex) { logger.Error ($"Unhandled exception while listening event {@event.Name} ", ex); }
            };
        }

        private static async Task UpdateMetrics (Configuration configuration, ILoanMetricsRepository loanMetricsRepository, EventInfo @event, string loanNumber, IEventHubClient eventHubService) {
            var eventInfo = configuration.MetricsEvent.Where (x => x.Name.ToLower () == @event.Name.ToLower ()).ToList ();
            foreach (var metrics in eventInfo) {
                var counter = 0;
                try {
                    counter = Convert.ToInt16 (metrics.Counter.FormatWith (@event));
                } catch (Exception) { }

                foreach (var item in metrics.MetricsToUpdte) {
                    var metricsDetails = await loanMetricsRepository.GetMetrics (loanNumber);
                    var updatedValue = 0;
                    if (metricsDetails != null) {
                        var countToUpdate = metricsDetails.Metrics?.FirstOrDefault (x => x.Name == item);
                        if (countToUpdate != null && metrics.IsResetCounter) {
                            countToUpdate.Value = 0;
                        } else if (countToUpdate != null) {
                            countToUpdate.Value = counter == 0 ? ++countToUpdate.Value : counter;
                            updatedValue = countToUpdate.Value;
                        } else {
                            metricsDetails.Metrics = metricsDetails.Metrics;
                            updatedValue = !metrics.IsResetCounter ? 1 : 0;
                            metricsDetails.Metrics.Add (new MetricsDetail () { Name = item, Value = updatedValue });

                        }
                        await loanMetricsRepository.AddOrUpdate (metricsDetails);
                    } else {
                        ILoanMetrics metricsData = new LoanMetrics ();
                        metricsData.LoanNumber = loanNumber;
                        metricsData.Metrics = new List<IMetricsDetail> ();
                        updatedValue = !metrics.IsResetCounter ? 1 : 0;
                        metricsData.Metrics.Add (new MetricsDetail () { Name = item, Value = updatedValue });
                        await loanMetricsRepository.AddOrUpdate (metricsData);
                    }
                    if (metrics.IsEventToBePublish) {
                        await eventHubService.Publish (item, new {
                            LoanNumber = loanNumber,
                                Counter = updatedValue
                        });
                    }

                }
            }

        }
    }
}