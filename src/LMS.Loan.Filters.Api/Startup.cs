﻿using LMS.Loan.Filters.Abstractions;
using LMS.Loan.Filters.Abstractions.Configurations;
using LMS.Loan.Filters.Persistence;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using LendFoundry.Security.Encryption;
using LMS.LoanManagement.Client;
using LendFoundry.StatusManagement.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.VerificationEngine.Client;
using LendFoundry.ProductRule.Client;
using LMS.LoanAccounting.Client;
using LMS.Payment.Processor.Client;
using LendFoundry.CallManagement.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.ProductConfiguration.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LMS.Loan.Filters.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "LoanFilters"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LMS.Loan.Filters.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<TaggingConfiguration>(Settings.TaggingEvents);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddConfigurationService<TaggingConfiguration>(Settings.TaggingEvents);
            services.AddTenantService();
            services.AddProductService();
            services.AddEventHub(Settings.ServiceName);
            services.AddLoanManagementService();
            services.AddStatusManagementService();
            services.AddDataAttributes();
            services.AddVerificationEngineService();
            services.AddProductRuleService();
            services.AddAccrualBalance();
            services.AddPaymentProcessorService();
            services.AddCallManagement();
            services.AddAchConfigurationService();
            //  interface resolvers
            // services.AddTransient(p =>
            // {
            //     var currentTokenReader = p.GetService<ITokenReader>();
            //     var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
            //     return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            // });
            services.AddMongoConfiguration(Settings.ServiceName);

            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<TaggingConfiguration>>().Get());
            //services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<ILoanFilterService, LoanFilterService>();
            services.AddTransient<IFilterViewRepository, FilterViewRepository>();
            services.AddTransient<IFilterViewRepositoryFactory, FilterViewRepositoryFactory>();
            services.AddTransient<ITagFilterRepository, TagFilterRepository>();
            services.AddTransient<ITagFilterRepositoryFactory, TagFilterRepositoryFactory>();
            services.AddTransient<ILoanFilterListener, LoanFilterListener>();
            services.AddTransient<ILoanFilterServiceFactory, LoanFilterServiceFactory>();
            services.AddTransient<ILoanMetricsRepository, LoanMetricsRepository>();
            services.AddTransient<ILoanMetricsRepositoryFactory, LoanMetricsRepositoryFactory>();

            services.AddEncryptionHandler();
            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
#if DOTNET2
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Loan Filters Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseLoanFilterListener();
            app.UseMvc();
        }
    }
}
