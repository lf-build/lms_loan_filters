﻿using LMS.Loan.Filters.Abstractions;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace LMS.Loan.Filters.Api.Controllers
{
    /// <summary>
    /// FilterController
    /// </summary>
    [Route("/")]
    public class FilterController : ExtendedController
    {
        /// <summary>
        /// FilterController constructor
        /// </summary>
        /// <param name="service"></param>
        public FilterController(ILoanFilterService service)
        {
            Service = service;
        }

        private ILoanFilterService Service { get; }

       /// <summary>
       /// GetAll
       /// </summary>
       /// <returns></returns>
        [HttpGet("/all")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        public IActionResult GetAll()
        {
            return Execute(() => Ok(Service.GetAll()));
        }

        /// <summary>
        /// GetAllByStatus
        /// </summary>
        /// <param name="statuses"></param>
        /// <returns></returns>
        [HttpGet("status/{*statuses}")]
        [ProducesResponseType(typeof(IFilterView[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAllByStatus(string statuses)
        {
            return ExecuteAsync(async () =>
            {
                statuses = WebUtility.UrlDecode(statuses);
                var listStatus = statuses?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return  Ok(await Service.GetAllByStatus(listStatus));
            });
        }

        /// <summary>
        /// ApplyTagsToApplication
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        [HttpPost("applytags/{loanNumber}/{*tags}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult ApplyTagsToApplication(string loanNumber, string tags)
        {
            try
            {
                return Execute(() =>
                {
                    tags = WebUtility.UrlDecode(tags);
                    var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    Service.TagsWithoutEvaluation(loanNumber, listTags);
                    return  Ok();
                });
            }
            catch (ArgumentNullException ex)
            {
                return new ErrorResult(404, ex.Message);
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        /// <summary>
        /// RemoveTagsFromApplication
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        [HttpPost("removetags/{loanNumber}/{*tags}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult RemoveTagsFromApplication(string loanNumber, string tags)
        {
            try
            {
                return Execute(() =>
                {
                    tags = WebUtility.UrlDecode(tags);
                    var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    Service.UnTagWithoutEvaluation(loanNumber, listTags);
                    return  Ok();
                });
            }
            catch (ArgumentNullException ex)
            {
                return new ErrorResult(404, ex.Message);
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        /// <summary>
        /// GetApplicationTagInfo
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <returns></returns>
        [HttpGet("tags/{loanNumber}")]
        [ProducesResponseType(typeof(IFilterViewTags), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetApplicationTagInfo(string loanNumber)
        {
            try
            {
                return Execute(() =>
                {
                    return  Ok(Service.GetApplicationTagInformation(loanNumber));
                });
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        /// <summary>
        /// GetMetrics
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <returns></returns>
        [HttpGet("matrics/{loanNumber}")]
        [ProducesResponseType(typeof(ILoanMetrics), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetMetrics(string loanNumber)
        {
            return ExecuteAsync(async () =>
            {
                return  Ok(await Service.GetMetrics(loanNumber));
            });
        }

        /// <summary>
        /// GetCountByTag
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        [HttpGet("/count/tag/{*tags}")]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetCountByTag(string tags)
        {
            return ExecuteAsync(async () =>
            {
                tags = WebUtility.UrlDecode(tags);
                var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return  Ok(await Service.GetCountByTag(listTags));
            });
        }

        /// <summary>
        /// GetTagInfoForApplicationsByTag
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        [HttpGet("tag/taginfo/{tag}")]
        [ProducesResponseType(typeof(IFilterViewTags[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetTagInfoForApplicationsByTag(string tag)
        {
            return ExecuteAsync(async () =>
            {
                return  Ok(await Service.GetTagInfoForApplicationsByTag(WebUtility.UrlDecode(tag)));
            });
        }

        /// <summary>
        /// GetAllMetrics
        /// </summary>
        /// <returns></returns>
        [HttpGet("/all/matrics")]
        [ProducesResponseType(typeof(List<ILoanMetrics>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetAllMetrics()
        {
            return Execute(() =>  Ok(Service.GetAllMetrics()));
        }

        /// <summary>
        /// GetloansByProductId
        /// </summary>
        /// <param name="productid"></param>
        /// <returns></returns>
        [HttpGet("/loan/{productid}")]
        [ProducesResponseType(typeof(IFilterView[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetloansByProductId(string productid)
        { 
            return await ExecuteAsync(async () => Ok(await (Service.GetLoansByProductId(productid))));
        }

        /// <summary>
        /// GetLoansByLoanNumbers
        /// </summary>
        /// <param name="loanNumbers"></param>
        /// <returns></returns>
        [HttpPost("/loans/loannumbers")]
        [ProducesResponseType(typeof(IFilterView[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoansByLoanNumbers([FromBody]List<string> loanNumbers)
        {
            return await ExecuteAsync(async () => Ok(await (Service.GetLoansByLoanNumbers(loanNumbers))));
        }

        /// <summary>
        /// GetCollectionData
        /// </summary>
        /// <param name="attemptRequest"></param>
        /// <returns></returns>
        [HttpPost("/collection/report")]
        [ProducesResponseType(typeof(List<CollectionResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetCollectionData([FromBody]LoanCollectionRequest attemptRequest)
        {
            return await ExecuteAsync(async () => Ok(await (Service.GetCollectionData(attemptRequest))));
        }

       /// <summary>
        /// GetFundingData
        /// </summary>
        /// <param name="attemptRequest"></param>
        /// <returns></returns>
        [HttpPost("/funding/report")]
        [ProducesResponseType(typeof(List<CollectionResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFundingData([FromBody]LoanCollectionRequest attemptRequest)
        {
            return await ExecuteAsync(async () => Ok(await (Service.GetFundingData(attemptRequest))));
        }

        

        /// <summary>
        /// GetFollowUpData
        /// </summary>
        /// <param name="attemptRequest"></param>
        /// <returns></returns>
        [HttpPost("/follow-up/report")]
        [ProducesResponseType(typeof(List<CollectionResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetFollowUpData([FromBody]LoanCollectionRequest attemptRequest)
        {
            return await ExecuteAsync(async () => Ok(await (Service.GetFollowUpData(attemptRequest))));
        }
        
        /// <summary>
        /// Get Loan Information By BorrowerId
        /// </summary>
        /// <param name="borrowerId"></param>
        [HttpGet("/borrower/{borrowerId}")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetByBorrowerId(string borrowerId)
        {
            try
            {
                return Ok(await Service.GetByBorrowerId(borrowerId));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// GetLoansByLoanNumbers
        /// </summary>
        /// <returns></returns>
        [HttpGet("/loans/on-boarded/{year}/{month}/{day}")]
        [ProducesResponseType(typeof(IFilterView[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoansByLoanNumbers(int year,int month,int day)
        {
            return await ExecuteAsync(async () => Ok(await (Service.GetLoansByOnBoardedByDate(year, month, day))));
        }
    }
}